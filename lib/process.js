const { spawn } = require('child_process');

exports.p1 = function (subjectName, cb) {
  console.log('---log', process.cwd());
  try {
    const cmdS = `subName=${subjectName} baseDir=${process.cwd()}/processedImages userImgDir=${process.cwd()}/images sh `
    console.log('cmdSS', cmdS);
    const dir = '/Users/yogesh/workspace/lil-projects/face_recognition/user_detection';
    const cmd = spawn('sh', [dir + '/p1.sh'], {
      env: {
        subName: subjectName,
        baseDir: `${process.cwd()}/processedImages`,
        userImgDir: `${process.cwd()}/images`
      }
    });

    cmd.stdout.on('data', (data) => {
      console.log(`stdout: ${data}`);
    });

    cmd.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
    });

    cmd.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
      cb(null, 1);
    });
  } catch (err) {
    console.log('exec err', err);
    cb(err);
  }
};
