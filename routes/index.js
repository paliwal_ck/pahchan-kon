var express = require('express');
var router = express.Router();
var fs = require('mz/fs');
const { p1 } = require('../lib/process');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Photo Saver'
  });
});

router.get('/api/p1', async (req, res, next) => {
  const name = req.query.userName;
  if (name) {
    p1(name, function (err, res) {
      console.log('--------------', err, res);
    });
  } else {
    res.send(422, {
      message: 'no name in processing'
    });
  }
});

router.post('/api/p1', async (req, res, next) => {
  const name = req.body.userName;
  if (name) {
    p1(name, function (err, res) {
      console.log('--------------', err, res);
    });
  } else {
    res.send(422, {
      message: 'no name in processing'
    });
  }
});

router.post('/api/photo', async (req, res, next) => {
  if (req.body.userName) {
    const exists = await fs.exists(`images/${req.body.userName}`)
    if (!exists) {
      await fs.mkdir(`images/${req.body.userName}`);
    }
    var imageBuffer = decodeBase64Image(req.body.data);
    fs.writeFile(`images/${req.body.userName}/${new Date().getTime()}.jpg`, imageBuffer.data, function (err) {
      if (err) {
        console.log('err', err)
        res.send(500, {
          message: 'internal server error'
        });
      } else {
        res.send(200, {
          message: 'done'
        });
      }

    });

  } else {
    res.send(422, {
      message: 'please provide user name'
    });
  }
});


function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};

  if (matches.length !== 3) {
    return new Error('Invalid input string');
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');

  return response;
}

module.exports = router;
