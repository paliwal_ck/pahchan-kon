// document.webkitExitFullscreen();
// document.mozCancelFullScreen();
// document.msExitFullscreen();
// document.exitFullscreen();

// var dom = document.getElementById("app-body");
// console.log(dom);
// dom.webkitRequestFullscreen();

// dom.mozRequestFullScreen();
// dom.msRequestFullscreen();
// dom.requestFullscreen();

function getContextMenu(e) {
  e.preventDefault();
}

function appFS() {
  // Launch fullscreen for browsers that support it!
  launchIntoFullscreen(document.documentElement); // the whole page
  // launchIntoFullscreen(document.getElementById("videoElement")); // any individual element
  window.addEventListener('keydown', function (ev) {
    console.log(ev);
    ev.preventDefault();
  });
}

// Find the right method, call on correct element
function launchIntoFullscreen(element) {
  if (element.requestFullscreen) {
    element.requestFullscreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
